

public class Alimento {
    private String nombre;
    private String Descripcion;
    private float cont_lipidos;
    private float cont_Hcarbono;
    private float cont_proteinas;
    private String origen;
    private String cont_vitaminas;
    private float v_energetico;
    
    
    public Alimento(String nombre, String Descripcion, float cont_lipidos, float cont_Hcarbono, float cont_proteinas,String origen, String cont_vitaminas ){
        this.nombre= nombre;
        this.Descripcion= Descripcion;
        this.cont_Hcarbono=cont_Hcarbono;
        this.cont_lipidos=cont_lipidos;
        this.cont_proteinas=cont_proteinas;
        this.cont_vitaminas=cont_vitaminas;
        this.origen=origen;
               
    }
    /**Metodo para mostrar los datos */
    public void MostrarDatos(){
        System.err.println("\nNombre Alimento: "+ this.nombre+ "\nDescripcion: "+ this.Descripcion
                +"\nCantidad Hidratos de Carbono: " +this.cont_Hcarbono+ "\nCantidad de Proteinas:" + this.cont_proteinas
                + "\nEs de Origen Animal?: "+ this.origen + "\nContenido de Vitaminas (A= Alto, M= Medio, B= bajo): " + this.cont_vitaminas);
    }
    
/**Metodo para saber si es un alimento recomendable */        
     public boolean esRecomendable() {
       boolean flag = false;
        if((getCont_proteinas() >= 10 && getCont_proteinas() <= 15)&&(getCont_Hcarbono() >= 55 && getCont_Hcarbono() <= 65)&&(getCont_lipidos() >= 30 && getCont_lipidos() <= 35)    ) {
            flag = true;
            System.out.println("Este alimento es recomendable para Deportistas");
        } else {
            System.out.println("Este alimento NO es recomendable para Deportistas");
        }
        return flag;
    }
    /** Metodo para determinar el valor energético */
     public float ValorEnergetico(){
         float valor = 0;
         valor = (float) ((getCont_lipidos()*9.4)+(getCont_Hcarbono()*4.1)+(getCont_proteinas()*5.3));//¡?
         System.out.println("Valor Energetico: "+valor);
         return valor;
     }
     /** Metodo para determinar si es dietetico */
     public boolean esDietetico(){
         boolean flag=false;
         if(getCont_lipidos()< 20 && (getCont_vitaminas().equals("A")) || (getCont_vitaminas().equals("M"))){//&& getCont_vitaminas() ==// PREGUNTAR VITAMINAS
             flag=true;
             System.out.println("Este alimento ES DIETETICO");
         } else {
             System.out.println("Este alimento NO ES DIETETICO");
         }
         return flag;
     }
    
            

    /**
     Retorna el nombre
     */
    public String getNombre() {
        return nombre;
    }

   
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
    Retorna la Descripcion
     */
    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String Descripcion) {
        this.Descripcion = Descripcion;
    }

    /**
    Retorna canidad de lipidos
     */
    public float getCont_lipidos() {
        return cont_lipidos;
    }

    public void setCont_lipidos(float cont_lipidos) {
        this.cont_lipidos = cont_lipidos;
    }

    /**
    Retorna la cantidad de hidratos de carbono
     */
    public float getCont_Hcarbono() {
        return cont_Hcarbono;
    }

 
    public void setCont_Hcarbono(float cont_Hcarbono) {
        this.cont_Hcarbono = cont_Hcarbono;
    }

    /**
    Retorna lacantidad de proteinas
     */
    public float getCont_proteinas() {
        return cont_proteinas;
    }

    
    public void setCont_proteinas(float cont_proteinas) {
        this.cont_proteinas = cont_proteinas;
    }

    /**
     Retorna el Orginen del alimento
     */
    public String getOrigen() {
        return origen;
    }

   
    public void setOrigen(String origen) {
        this.origen = origen;
    }

    /**
    Retorna Cant Vitaminas
     */
    public String getCont_vitaminas() {
        return cont_vitaminas;
    }

  
    public void setCont_vitaminas(String cont_vitaminas) {
        this.cont_vitaminas = cont_vitaminas;
    }

    /**
     Retornara el valor energetico
     */
    public float getV_energetico() {
        return v_energetico;
    }

       
    public void setV_energetico(float v_energetico) {
        this.v_energetico = v_energetico;
    }

    
    
}

    
