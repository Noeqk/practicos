public class App {
    public static void main(String[] args) throws Exception {
    
      Alimento ali = new Alimento("Choclo", "Choclo en grano", 6, 5, 3, "M", "No"  );
      Alimento ali2 = new Alimento("Jugo", "Jugo de Soja", 2, 7, 2, "M", "No"  );
           
      ali.esDietetico();
      ali.esRecomendable();
      ali.ValorEnergetico();
      ali.MostrarDatos();

      ali2.esDietetico();
      ali2.esRecomendable();
      ali2.ValorEnergetico();
      ali2.MostrarDatos();
        
    }
}
