

public class Automovil {
    private String patente;
    private String marca;
    private String modelo;
    private int kilometraje;
    

    Automovil (String patente, String marca, String modelo, int kilometraje){
        this.patente=patente;
        this.marca=marca;
        this.modelo=modelo;
        this.kilometraje= kilometraje;

    }
    
    public String MotivoReparacion(){
        String motivo ="Cambio de Respuesto";
        System.out.println("Motivo de Reparación" + motivo);
        return motivo;
    }

    public void ObtenerDatoAuto(){
       
        System.out.println("DATOS AUTO");
        System.out.println("Patente" +this.getPatente());
        System.out.println("Marca" +this.getMarca());
        System.out.println ("\n");
    }


        
   



    /**
     * @return String return the patente
     */
    public String getPatente() {
        return patente;
    }

    /**
     * @param patente the patente to set
     */
    public void setPatente(String patente) {
        this.patente = patente;
    }

    /**
     * @return String return the marca
     */
    public String getMarca() {
        return marca;
    }

    /**
     * @param marca the marca to set
     */
    public void setMarca(String marca) {
        this.marca = marca;
    }

    /**
     * @return String return the modelo
     */
    public String getModelo() {
        return modelo;
    }

    /**
     * @param modelo the modelo to set
     */
    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    /**
     * @return int return the kilometraje
     */
    public int getKilometraje() {
        return kilometraje;
    }

    /**
     * @param kilometraje the kilometraje to set
     */
    public void setKilometraje(int kilometraje) {
        this.kilometraje = kilometraje;
    }

}