

public class Propietario {
  
    private int nroDni;
    private String nombre;
    private String apellido;
    private String domicilio;
    private int telefono;

    /**
Constructor
     */
    public Propietario (int nroDni, String nombre, String apellido, String domicilio, int telefono){
        this.nroDni=nroDni;
        this.nombre=nombre;
        this.apellido=apellido;
        this.domicilio=domicilio;
        this.telefono=telefono;
    }
  

    

    public void MostrarDatos(){
        System.err.println("\nDATOS DEL PROPIETARIO: "+"\nNumero Dni: "+ this.getNroDni()+"\nNombre Propietario: "+ this.getNombre()+ "\nApellido: "+ this.getApellido()
                +"\nDomicilio: " +this.getDomicilio()+ "\nTelefono:" + this.getTelefono());
    }
    

   

    public int getNroDni() {
        return nroDni;
    }
    /**
     * @param nroDni the nroDni to set
     */
    public void setNroDni(int nroDni) {
        this.nroDni = nroDni;
    }

    /**
     * @return String return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public String getApellido() {
        return apellido;
    }
    /**
     * @param apellido the apellido to set
     */
    public void setApellido(String apellido) {
        this.apellido = apellido;
    }
    public String getDomicilio() {
        return domicilio;
    }
    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    /**
     * @return int return the telefono
     */
    public int getTelefono() {
        return telefono;
    }

    /**
     * @param telefono the telefono to set
     */
    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }

}