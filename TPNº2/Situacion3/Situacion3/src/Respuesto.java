
public class Respuesto {
    private String codigo;
    private String nombre;
    private int cantidad;
    private float precioResp;


    public Respuesto (String codigo, String nombre, int cantidad, float precioResp){
        this.codigo = codigo;
        this.nombre = nombre;
        this.cantidad = cantidad;
        this.precioResp=precioResp;

    }
 

    public float CostoResp(int cantidad, float precioResp){
        float valorResp = 0;
        valorResp = (float) (getCantidad()*getPrecioResp());//¡?
        System.out.println("Costo de Respuesto: "+valorResp);
        return valorResp;
    }

    


    /**
     * @return String return the codigo
     */
    public String getCodigo() {
        return codigo;
    }

    /**
     * @param codigo the codigo to set
     */
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    /**
     * @return String return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return int return the cantidad
     */
    public int getCantidad() {
        return cantidad;
    }

    /**
     * @param cantidad the cantidad to set
     */
    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    /**
     * @return float return the precioResp
     */
    public float getPrecioResp() {
        return precioResp;
    }

    /**
     * @param precioResp the precioResp to set
     */
    public void setPrecioResp(float precioResp) {
        this.precioResp = precioResp;
    }

}