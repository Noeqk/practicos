

public class FichaTecnica {

    private String codReparacion;
    private String tarea;
    private float costoRepa;
    private String motivo;

    public FichaTecnica (String codReparacion, String tarea, float costoRepa, String motivo){
        this.codReparacion=codReparacion;
        this.tarea=tarea;
        this.costoRepa=costoRepa;
        this.motivo=motivo;
    }
    

    Respuesto costo = new Respuesto("24A", "correa de distribución", 1, 6400f );
    
    public float CostoReparacion(){
        float valor = 0;

        valor = (float) (getCostoRepa()+costo.CostoResp(1, 6400f));//¡?
        System.out.println("Costo de Reparación: "+valor);
        return valor;
    }

    public String MotivoReparacion(){
        String motivo ="Cambio de Respuesto";
        System.out.println("Motivo de Reparación:" + "  " + motivo);
        return motivo;
    }


    

    /**
     * @return String return the codReparacion
     */
    public String getCodReparacion() {
        return codReparacion;
    }

    /**
     * @param codReparacion the codReparacion to set
     */
    public void setCodReparacion(String codReparacion) {
        this.codReparacion = codReparacion;
    }

    /**
     * @return String return the tarea
     */
    public String getTarea() {
        return tarea;
    }

    /**
     * @param tarea the tarea to set
     */
    public void setTarea(String tarea) {
        this.tarea = tarea;
    }

    /**
     * @return float return the costoRepa
     */
    public float getCostoRepa() {
        return costoRepa;
    }

    /**
     * @param costoRepa the costoRepa to set
     */
    public void setCostoRepa(float costoRepa) {
        this.costoRepa = costoRepa;
    }

    /**
     * @return String return the motivo
     */
    public String getMotivo() {
        return motivo;
    }

    /**
     * @param motivo the motivo to set
     */
    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

}