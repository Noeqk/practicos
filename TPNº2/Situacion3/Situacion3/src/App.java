public class App {
    private static float costoReparacion;

    public static void main(String[] args) throws Exception {
        System.out.println("Hello, World!");
        
        
        Propietario p = new Propietario(3333333, "Juan", "Cordoba", "La Carrera", 154263722 );
        p.MostrarDatos();

        Automovil a = new Automovil("AC123","Ford", "Falcon", 2000);
        a.ObtenerDatoAuto();
        
        FichaTecnica ft= new FichaTecnica("123A", "Cambio Respuesto", 2500f, "Rotura de respuesto");
        ft.CostoReparacion();
        ft.MotivoReparacion();
    }
}
