 // package io.github.jiangdequan;

public class Socio {
    private String apellido;
    private String nombre;
    private int dni;
    private int antiguedad;
    private String sociotipo;
    
    private int tiposocio=1;
   
    private int total;
    SocioComun tiposocComun ;
    Adherente tiposocadherente;


    
    Socios(String apellido, String nombre,int dni,int antiguedad,int tiposocio,SocioComun tiposocComun, 
    Adherente tiposocaderente ){
    this.apellido= apellido;
    this.nombre= nombre;
    this.dni=dni;
    this.antiguedad= antiguedad;
    this.tiposocio=tiposocio;
    this.tiposocComun = tiposocComun;
    this.Adherente = tiposocadherente;
    
            }
    
    public void setApellido(String apellido){
            this.apellido= apellido;
    }
    public void setNombre(String nombre){
        this.nombre= nombre;
    }
      public void setDni(int dni){
      this.dni=dni;
      }
      
     public void setAntiguedad(int antiguedad) {
     this.antiguedad= antiguedad;
     }
     
     public String getApellido(){
            return apellido;
    }
    public String getNombre(){
        return nombre;
    }
      public int getDni(){
      return dni;
      }
      
     public int getAntiguedad() {
     return antiguedad;
     }
    
   
    public String getSociotipo() {
        return sociotipo;
    }

   
    public void setSociotipo(String sociotipo) {
        this.sociotipo = sociotipo;
    }

    
    public int getTiposocio() {
        return tiposocio;
    }

   
    public void setTiposocio(int tiposocio) {
        this.tiposocio = tiposocio;
    }

    public int getTotal() {
        return total;
    }

    
    public void setTotal(int total) {
        this.total = total;
    }

   
    public SocioComun getTiposocComun() {
        return tiposocComun;
    }

    
    public void setTiposocComun(SocioComun tiposocComun) {
        this.tiposocComun = tiposocComun;
    }
    
    
   public void Imprimircomprobante(){
      
       switch(tiposocio){
           case 1:
             total= getTiposocComun().CalcularDeuda();
           
               break;
           case 2:
              
                total= getTiposocAdherente().CalcularDeuda();
               break;   
       }
       
       System.out.println("Nombre:"+""+getNombre());
       System.out.println("Apellido:"+""+getApellido());
       System.out.println("Categoria:"+""+getSociotipo());
       System.out.println(getTotal());
       
       if (getTiposocio()!= 0) {
           setTotal(getTiposocComun().CalcularDeuda());
           System.out.println("Monto Abonado:"+""+total);
       }
   }

    public Socioespecial getTiposocAdherente() {
        return tiposocadherente;
    }
    
}