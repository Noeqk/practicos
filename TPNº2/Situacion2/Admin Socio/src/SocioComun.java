 // package io.github.jiangdequan;

public class SocioComun extends Socio{
    private  int montoCuotaComun;
    private int CuotasAdeuda;
    
  
public SocioComun(int montoCuotaComun,int CuotasAdeuda,String apellido, String nombre,int dni,int antiguedad,int tiposocio,SocioComun tiposocComun, 
    Socioespecial tiposocEspecial){
    super(apellido, nombre,dni,antiguedad,tiposocio,tiposocComun,tiposocEspecial);
    
    this.montoCuotaComun=montoCuotaComun;
    this.CuotasAdeuda=CuotasAdeuda;
}  

public void setmontoCuotaComun(int montoCuotaComun){
    this.montoCuotaComun=montoCuotaComun;
    }
    
    public int getmontoCuotaComun(){
    return montoCuotaComun;
    }
   
    public int getCuotasAdeuda() {
        return CuotasAdeuda;
    }
    
    public void setCuotasAdeuda(int CuotasAdeuda) {
        this.CuotasAdeuda = CuotasAdeuda;
    }

    public int CalcularDeuda(){
        int deuda=0;
        deuda= montoCuotaComun * CuotasAdeuda;
        
        return deuda;//montoCuotaComun * CuotasAdeuda;
 }
}