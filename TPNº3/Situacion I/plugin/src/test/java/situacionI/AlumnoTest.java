package situacionI;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.gradle.internal.impldep.org.junit.Test;

public class AlumnoTest {
    
        Estudiante e1 = new Estudiante("Matias", 1023, 21, 38628166);
        public AlumnoTest() {
            Principal ges = new Principal();
           
            e1.inscribirAsignatura( new Asignatura("AMII"));
            e1.inscribirAsignatura( new Asignatura("Fisica"));
            e1.inscribirAsignatura( new Asignatura("Algebra"));
            e1.ponerNota(5, "AMII");
            e1.ponerNota(4, "Fisica");
            e1.ponerNota(10, "Algebra");
        }

        /**
         * Test of getNombre method, of class Alumno.
         */
    @Test
        public void testGetNombre() {
            
            assertEquals("Matias", e1.getApeynom());
            
        }
    
        
    
        /**
         * Test of getEdad method, of class Alumno.
         */
        @Test
        public void testGetEdad() {
            Integer a = 20;
            assertEquals( a , e1.getEdad());
            
        }
    
        /**
         * Test of getDocumento method, of class Alumno.
         */
        @Test
        public void testGetDocumento() {
            Integer b = 38628166;
            assertEquals(   b   , e1.getDni());
        }
    
        /**
         * Test of promedio method, of class Alumno.
         */
        @Test
        public void testPromedio() {
            double esperado = 7.0;
            assertEquals( esperado , e1.promedio(), 0.6);
            
        }
         
            
}