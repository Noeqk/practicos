package situacionI;

public class Asignatura {
    private final String nombre;
    private Integer nota;
    private boolean aprobado;

    public Asignatura(String nombre) {
        this.nombre = nombre;
        this.nota = 0;
        this.aprobado = false;
    }
 /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    
    /**
     * @return the nota
     */
    public Integer getNota() {
        return nota;
    }

    /**
     * @param nota the nota to set
     */
    public void setNota(Integer nota) {
        if (nota>0 && nota < 4) {
            this.nota = nota;
            this.aprobado = false;
        }
        else if(nota >= 4 && nota <11){
            this.aprobado = true;
            this.nota = nota;
        }
        
    }

    /**
     * @return the aprobado
     */
    public String isAprobado() {
        if (!this.aprobado) {
            return "Desaprobado";
        } else {
            return "Aprobado";
        }
        
    }
    
    @Override
    public String toString(){
        if(this.nota != 0)
            return "Nombre Asignatura: "+this.nombre+"\nNota: "+this.nota+"\n"+this.isAprobado();
        else return "Nombre Asignatura: "+this.nombre+"\nNota: No tiene"+"\n"+this.isAprobado();
    }

    
    
}