package situacionI;
import java.util.ArrayList;



public class Estudiante {
        private String apeynom;
        private int matricula;
        private int edad;
        private Integer dni;
        private Domicilio domicilio;
        private boolean inscripto;
        private ArrayList<Asignatura> materias = null;

        public Estudiante (String apeynom, int matricula, int edad, Integer dni) {
            this.apeynom = apeynom;
            this.matricula=matricula;
            this.edad = edad;
            this.dni=dni;
          
        }
    public Estudiante (String apeynom, int matricula, int edad, Integer dni, Domicilio domicilio) {
        this.apeynom = apeynom;
        this.matricula=matricula;
        this.edad = edad;
        this.dni=dni;
        this.domicilio=domicilio;
      
    }
    public Estudiante() {
    }


//Metodo para mostrar los datos
    public void mostrar(){
        if(this.isInscripto()){
            System.out.println("Nombre: "+this.apeynom+"\nMatricula: "+this.matricula+"\nEdad: "+this.edad+"\n Nro DNI:"+this.dni + "\nDomicilio" + this.getDomicilio());
         
        }   
    }
    
  
    public double promedio(){
        Integer promedio = 0 , i = 0;
        for (Asignatura materia : this.materias) {
            if(!(materia.getNota() == 0)){
                promedio = promedio + materia.getNota();
                i++;
            }
        }
        if(i == 0) return 0.0;
        return promedio/i;
    }


    //Inscribir en materias

    public void inscribirAsignatura(Asignatura newAsi){
        if(this.isInscripto())      this.materias.add(newAsi);
        else System.out.println("Inscribir");
    }

    

    //Asignar notas

    public void ponerNota(Integer nota, String materia){
        if(this.isInscripto()){
            Integer indice = this.buscar(materia);
            if(indice == null){
                System.out.println("El alumno no esta inscripto en esta Asignatura");
            }
            else{
                Asignatura asig = this.materias.get(indice);
                asig.setNota(nota);
            }
        }
        else System.out.println("Debe Inscribirse");
    }

    public Integer buscar(String materia){
        int i = 0;
        for (Asignatura materia1 : materias) {
            
            if (materia1.getNombre().equals(materia)) {
                return i;
            }
            i++;
        }
        return null;
    }
 
    
    
    /**
     * @return String return the apeynom
     */
    public String getApeynom() {
        return apeynom;
    }

    /**
     * @param apeynom the apeynom to set
     */
    public void setApeynom(String apeynom) {
        this.apeynom = apeynom;
    }

    /**
     * @return int return the matricula
     */
    public int getMatricula() {
        return matricula;
    }

    /**
     * @param matricula the matricula to set
     */
    public void setMatricula(int matricula) {
        this.matricula = matricula;
    }

    /**
     * @return int return the edad
     */
    public int getEdad() {
        return edad;
    }

    /**
     * @param edad the edad to set
     */
    public void setEdad(int edad) {
        this.edad = edad;
    }

    /**
     * @return Integer return the dni
     */
    public Integer getDni() {
        return dni;
    }

    /**
     * @param dni the dni to set
     */
    public void setDni(Integer dni) {
        this.dni = dni;
    }

    public boolean isInscripto() {
        return inscripto;
    }

    /**
     * @param inscripto the inscripto to set
     */
    public void setInscripto(boolean inscripto) {
        this.inscripto = inscripto;
    }

    /**
     * @return Domicilio return the domicilio
     */
    public Domicilio getDomicilio() {
        return domicilio;
    }

    /**
     * @param domicilio the domicilio to set
     */
    public void setDomicilio(Domicilio domicilio) {
        this.domicilio = domicilio;
    }

}