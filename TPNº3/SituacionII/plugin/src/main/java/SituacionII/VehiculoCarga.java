package SituacionII;

public class VehiculoCarga extends Vehiculo{
    private int km;
    
    public  VehiculoCarga(String marca, String patente, float precioBase, int km){
        super (marca,patente, precioBase);
        this.km=km;
    }
    

    /**
     * @return int return the km
     */
    public int getKm() {
        return km;
    }

    /**
     * @param km the km to set
     */
    public void setKm(int km) {
        this.km = km;
    }

}