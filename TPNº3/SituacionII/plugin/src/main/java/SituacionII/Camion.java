package SituacionII;

public class Camion extends VehiculoCarga{

    public Camion (String marca, String patente,float precioBase, int km){
        super (marca, patente,precioBase,km);

        
    }

    public float alquiler (int numDias, int km){
        if (km<50){
            return super.alquiler(numDias)+ (300*numDias) + 200;
        }else {
            return super.alquiler(numDias)+ ((20*km)*numDias) + 200;
    
        }

    }
    
}