package SituacionII;

public class Vehiculo {
    private String marca;
    private String patente;
    private float precioBase;
  

    public Vehiculo(String marca, String patente,float precioBase){
        this.marca=marca;
        this.patente=patente;
        this.precioBase=precioBase;
        
    }

    public Vehiculo(){

    }
    public  float alquiler(int numDias){

        return precioBase *numDias;
    }

    public String toString (){
        return "Automovil [marca =" + marca +"]";
    }



   
    

    /**
     * @return String return the marca
     */
    public String getMarca() {
        return marca;
    }

    /**
     * @param marca the marca to set
     */
    public void setMarca(String marca) {
        this.marca = marca;
    }

    /**
     * @return String return the patente
     */
    public String getPatente() {
        return patente;
    }

    /**
     * @param patente the patente to set
     */
    public void setPatente(String patente) {
        this.patente = patente;
    }

}